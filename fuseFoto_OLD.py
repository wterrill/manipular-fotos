import sys
from PIL import Image

images = list(map(Image.open, ['01004833.jpeg', '04800216.jpeg', '2x.png']))
#image = Image.open('01004833.jpeg')
#offer = Image.open('01004833.jpeg')
widths, heights = zip(*(i.size for i in images))

total_width = sum(widths)
max_height = max(heights) 

new_im = Image.new('RGB', (total_width, max_height))

x_offset = 0
for im in images:
  new_im.paste(im, (x_offset,0))
  x_offset += im.size[0]

new_im = new_im.resize((450,450), Image.ANTIALIAS)
new_im.save('test2.jpg')
