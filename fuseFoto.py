from PIL import Image, ImageOps
import urllib.request
import io
import pandas as pd
import numpy as np
import os
import sys


config_name = 'Fotos_SKU.xlsx'
print("loading file: " + config_name)

# determine if application is a script file or frozen exe
if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(__file__)

config_path = os.path.join(application_path, config_name)

df = pd.read_excel(config_path)
#df = pd.read_excel("Copia de Productos 2x 3x.xlsx","Sheet1")

sku_list = df['SKU'].tolist()

for index, row in df.iterrows():
  SKU = str(row['SKU'])

  #SKU = "2000084"
  #SKU= "20013877"
  #SKU = "4800086"
  while len(SKU) < 8:
    SKU = "0" + SKU
  TYPE = row['FOTO']
  URL=row['LINK FOTO']
  if URL[0] == "h":
    fd = urllib.request.urlopen(URL)
    print("Downloading picture for SKU = " + SKU + " using link: " + URL)
  else:
    fd = urllib.request.urlopen("http://s7d2.scene7.com/is/image/Tottus/"+SKU)
    print("Downloading picture for SKU = " + SKU + " using link: http://s7d2.scene7.com/is/image/Tottus/"+SKU)
  image = Image.open(io.BytesIO(fd.read()))
  imageBox = ImageOps.invert(image).getbbox()
  print("cropping image")
  cropped=image.crop(imageBox)
  
  if cropped.size[1] > cropped.size[0]:
    #resize height
    orig_height = cropped.size[1]
    orig_width = cropped.size[0]
    final_height = 450
    ratio = final_height / orig_height
    final_width = int(orig_width * ratio)
  if cropped.size[1] < cropped.size[0]:
    #resize height
    orig_height = cropped.size[1]
    orig_width = cropped.size[0]
    final_width = 450
    ratio = final_width / orig_width
    final_height = int(orig_height * ratio)

  cropped = cropped.resize((final_width, final_height), Image.ANTIALIAS)

  print("loading " + TYPE + " image")
  offer = Image.open(application_path + "/" + TYPE + '.png')
  offer.thumbnail((100,100), Image.ANTIALIAS)

  # https://s7d2.scene7.com/is/image/Tottus/04800217
  #offer = offer.resize((450,450), Image.ANTIALIAS)

  #image.thumbnail(size, Image.ANTIALIAS)
  # new = Image.new('RGBA', size, (255, 0, 0, 255))  #without alpha, red
  new = Image.new('RGBA', (450,450), (255, 255, 255, 255))  #with alpha
  #new.paste(image,(int((450 - image.size[0]) / 2), int((450 - image.size[1]) / 2)))

  if cropped.size[1] > cropped.size[0]:
    paste_here = (int(225 - cropped.size[0] / 2),0)
  else:
    paste_here = (0, int(225 - cropped.size[1] / 2))
  new.paste(cropped, paste_here)
  new.paste(offer, (0,0))

  print("saving new picture: /results/" + TYPE + '-'+ SKU + '.png')
  new.save(application_path + "/results/" + TYPE + '-'+ SKU + '.png')
